import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FaPlus from 'react-icons/lib/fa/plus';
import TiUserAdd from 'react-icons/lib/ti/user-add';
import TiUserAddOutline from 'react-icons/lib/ti/user-add-outline';
import TiDelete from 'react-icons/lib/ti/delete';

export class Users extends Component {
    constructor(props) {
        super(props);

        this.state = {
            formValues: {
                newName: "",
                newSurname: ""
            },
            isToggleOn: false
        };
        this.state.users = props.users;
    }

    handleClick() {
        console.log(this.state.isToggleOn);
        this.setState(prevState => ({
            isToggleOn: !prevState.isToggleOn
        }));
    }

    handleChange(event) {
        event.preventDefault();
        let formValues = this.state.formValues;
        let name = event.target.name;
        let value = event.target.value;

        formValues[name] = value;

        this.setState({formValues})
    }

    handleAddEvent(event) {
        event.preventDefault();
        console.log(this.state.formValues);

          var id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
          var user = {
              id: id,
              name: this.state.formValues.newName,
              surname: this.state.formValues.newSurname,
          };
          this.state.users.push(user);
          // update state
          this.setState(this.state.users);
    }

    handleRowEdit(user) {
        var index = this.state.users.indexOf(user);
        this.setState(this.state.users);
    }

    handleRowDelete(user) {
        var index = this.state.users.indexOf(user);
        this.state.users.splice(index, 1);
        this.setState(this.state.users);
    }

    handleUserTable(evt) {
        var item = {
            id: evt.target.id,
            name: evt.target.name,
            value: evt.target.value
        };
        var users = this.state.users.slice();
        var newUsers = users.map(function(user) {

            for (var key in user) {
                if (key === item.name && user.id === item.id) {
                    user[key] = item.value;
                }
            }
            return user;
        });
        this.setState({users:newUsers});
        console.log(this.state.users);
    };
    render() {
        return (
          <div className="user-list">
                <UserTable onUserTableUpdate={this.handleUserTable.bind(this)}
                       onClick={this.handleClick.bind(this)}
                       onRowAdd={this.handleAddEvent.bind(this)}
                       onRowEdit={this.handleRowEdit.bind(this)}
                       onRowDel={this.handleRowDelete.bind(this)}
                       users={this.state.users}
                       newName = {this.state.formValues.newName}
                       newSurname = {this.state.formValues.newSurname}
                       isToggleOn = {this.state.isToggleOn}
                />
            </div>
        );
    }
}

class UserTable extends Component {
    onRowAdd(){
        this.props.onRowAdd();
    }
    render() {
        var onUserTableUpdate = this.props.onUserTableUpdate;
        var newClick = this.props.onClick;
        var rowDel = this.props.onRowDel;
        var rowEdit = this.props.onRowEdit;
        var isToggleOn = this.props.isToggleOn;
        var rowAdd = this.props.onRowAdd;
        var newName = this.props.newName;
        var newSurname = this.props.newSurname;
        var user = this.props.users.map(function(user){
            return (<UserRow user={user}
                             onEditEvent={rowEdit.bind(this)}
                             onDelEvent={rowDel.bind(this)}
                             onUserTableUpdate={onUserTableUpdate}
                             key={user.id} />)
        });
        return (
            <div className="user-list">
                <div className="new-user">
                    <button className="btn btn-left" onClick={newClick}>
                        <TiUserAddOutline className="new-user-btn"/>
                        <span className="new-user-label">Add new user</span>
                    </button>
                </div>

                <table className="tbl">
                    <thead>
                        <tr className="row">
                            <th className="cellHeader cell large-cell">Name</th>
                            <th className="cellHeader cell large-cell">Surname</th>
                            <th className="cellHeader cell small-call"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {user}
                    </tbody>
                </table>
                <br />
                { isToggleOn ? <AddForm onUserTableUpdate={onUserTableUpdate}
                                      onRowAdd={rowAdd}
                                      newName = {newName}
                                      newSurname = {newSurname}
                                      isToggleOn = {isToggleOn}/> : null }
            </div>
        );
    }
}

class AddForm extends  Component {
    onRowAdd(){
        this.props.onRowAdd();
    }
    render(){
        var rowAdd = this.props.onRowAdd;
        var newName = this.props.newName;
        var newSurname = this.props.newSurname;
        return(
            <form className="form" onSubmit={rowAdd}>
                <label className="form-label">
                    Name:
                    <input className="form-input-box" value={newName} onChange={this.handleChange} />
                </label>
                <label className="form-label">
                    Surname:
                    <input className="form-input-box" value={newSurname} onChange={this.handleChange} />
                </label>
            </form>
        )
    }
}

class UserRow extends Component {

    onEditEvent(){
        this.props.onEditEvent(this.props.user);
    }

    onDelEvent(){
        this.props.onDelEvent(this.props.user);
    }

    render(){
        var rowDel = this.props.onDelEvent;
        return(
            <tr className="eachRow row">
                <Cell
                    cellData={{
                        type:"name",
                        value: this.props.user.name,
                        id: this.props.user.id
                    }}
                />
                <Cell
                    cellData={{
                        type:"surname",
                        value: this.props.user.surname,
                        id: this.props.user.id
                    }}
                />
                <td className="del-cell">
                    <button className="btn del-btn" onClick={rowDel}>
                        <TiDelete />
                    </button>
                </td>
            </tr>
        );
    }
}

class Cell extends React.Component {

    render() {
        return (
            <td className="cell cell-data">
                {this.props.cellData.value}
            </td>
        );
    }
}

class EditableCell extends React.Component {

    render() {
        return (
            <td>
                <input type='text'
                       name={this.props.cellData.type}
                       id={this.props.cellData.id}
                       value={this.props.cellData.value}
                       onChange={this.props.onUserTableUpdate}/>
            </td>
        );
    }
}

Users.propTypes = {
    users: PropTypes.arrayOf(PropTypes.object)
};