import React, { Component } from 'react';
import {Users} from './Users'

export class App extends Component {
    constructor() {
        super();

        this.state = {
            userList: [
                { id: 1, name: 'George', surname: 'Roma' },
                { id: 2, name: 'Laura', surname: 'Balart' },
                { id: 3, name: 'Santiago', surname: 'Morales' },
                { id: 4, name: 'Laura', surname: 'Medina' }
            ]
        }
    }

    render() {
          return (
                <div className="App">
                    <header className="App-header">
                        <br />
                        <h1 className="App-title">User list</h1>
                    </header>
                    <br />
                    <div className="user-list">
                        <Users users={this.state.userList}/>
                    </div>
                </div>
          )
    }
}
